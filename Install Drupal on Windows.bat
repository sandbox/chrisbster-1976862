@ECHO off

REM Check to make sure that you are the administrator - exit if not
AT > NUL
IF %ERRORLEVEL% EQU 0 (
    ECHO You are Administrator. Confirmed.
)ELSE (
    ECHO You are NOT Administrator. Exiting...
	cscript alert.vbs "You did not run this file as administrator. Right click on 'Install Drupal on Windows' and select 'Run as Administrator'" >nul 2>&1
    GOTO END
)

REM Check to see if C:\wamp exists - if not, go to end
IF NOT EXIST C:\wamp (
	ECHO WAMP is not installed yet. Open up the CD and run 'WAMP Installer.exe'
	cscript alert.vbs "WAMP is not installed yet. Open up the CD and run 'WAMP Installer.exe'" >nul 2>&1
	GOTO END
)
REM Shutdown WAMP so that we can play with the directory structure without deadlocks
ECHO Shutting down WAMP if turned on
taskkill /im "wampmanager.exe" /f >nul 2>&1
net stop wampapache >nul 2>&1
net stop wampmysqld >nul 2>&1

REM Just in case there are going to be conflicts, delete the old web root
ECHO Deleting any remnants of old web root
RMDIR C:\wamp\www /S /Q  >nul 2>&1

REM Throw in the drupal file structure into C:\wamp\www
ECHO Copying Drupal file structure into C:\wamp\www
XCOPY "%~dp0Drupal File Structure" C:\wamp\www /E /I  1>nul

REM Remove any database that might already exist
ECHO Deleting any remnants of the old database
RMDIR C:\wamp\bin\mysql\mysql5.5.24\data /S /Q  >nul 2>&1

REM Throw in the clean database into C:\wamp\
ECHO Copying in clean Drupal database
XCOPY "%~dp0Drupal WAMP Database" C:\wamp\bin\mysql\mysql5.5.24\data /E /I 1>nul

REM Remove the existing httpd.conf
Echo Removing existing Apache configuration
DEL C:\wamp\bin\apache\apache2.2.22\conf\httpd.conf /Q /F  >nul 2>&1

REM Overwrite httpd.conf to change bind for IPv6 and turn rewrite module ON
ECHO Copying new Apache configuration into the correct directory
COPY "%~dp0Drupal Apache Configuration\httpd.conf" "C:\wamp\bin\apache\apache2.2.22\conf\httpd.conf" /Y 1>nul

ECHO Starting Wamp
START C:\wamp\wampmanager.exe
net start wampapache >nul 2>&1
net start wampmysqld >nul 2>&1

:End